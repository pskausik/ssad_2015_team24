## Date and Time
26 September, 7:30pm

##Attendees
Dhruv Khattar
Siddhartha Gairola
Ponnapalli Kausik
Vishnu Panjala

## Minutes
30 minutes

## Discussion
We went over the project in detail and decided which team member will be responsible for what task.

It was decided that Dhruv and Siddhartha will be handling the Automation part i.e. Continuous Integration and Continuous Deployment.
And Kausik and Vishnu will be working on the Testing part.

## Action Points
* Learn about GruntJs and TeamCity for Automation.
* Learn About JasmineJs for generating Test Cases.
