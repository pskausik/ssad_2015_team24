## Date and Time
27 September, 5:30pm

##Attendees
Dhruv Khattar
Siddhartha Gairola

## Minutes
30 minutes

## Discussion
Discussed with the ta the tasks we had been assigned by our client to our team members.
Were given tips as to how we should go about it.
## Action Points
* Learn about GruntJs and TeamCity for Automation.
* Learn About JasmineJs for generating Test Cases.
