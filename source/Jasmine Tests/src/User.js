function User() {
}
User.prototype.sign = function(log) {
      this.currentlySigningLog = log;
        this.isSigning = true;
};

User.prototype.change = function() {
      this.isSigning = false;
};

User.prototype.exit = function() {
      if (this.isSigning) {
              throw new Error("Please save changes before exiting!");
                }

        this.isSigning = true;
};

User.prototype.makeFavorite = function() {
      this.currentlySigningLog.persistFavoriteStatus(true);
};
