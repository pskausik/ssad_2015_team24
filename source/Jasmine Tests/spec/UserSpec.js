describe("User", function() {
  var user;
  var log;

  beforeEach(function() {
    user = new User();
    log = new Log();
  });

  it("User Should be able to log in", function() {
    user.sign(log);
    expect(user.currentlySigningLog).toEqual(log);

    //demonstrates use of custom matcher
    expect(user).toBeSigning(log);
  });

  describe("User has logged in", function() {
    beforeEach(function() {
      user.sign(log);
      user.change();
    });

    it("Indicate that the user is logged in.", function() {
      expect(user.isSigning).toBeFalsy();

      // demonstrates use of 'not' with a custom matcher
      expect(user).not.toBeSigning(log);
    });

    it("should allow the user to log out", function() {
      user.exit();
      expect(user.isSigning).toBeTruthy();
      expect(user.currentlySigningLog).toEqual(log);
    });
  });

  // demonstrates use of spies to intercept and test method calls
  it("shows the last log in", function() {
    spyOn(log, 'persistFavoriteStatus');

    user.sign(log);
    user.makeFavorite();

    expect(log.persistFavoriteStatus).toHaveBeenCalledWith(true);
  });

  //demonstrates use of expected exceptions
  describe("Log Out", function() {
    it("should throw an exception if changes not saved", function() {
      user.sign(log);

      expect(function() {
        user.exit();
      }).toThrowError("Please save changes before exiting!");
    });
  });
});
