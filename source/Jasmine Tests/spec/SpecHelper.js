beforeEach(function () {
  jasmine.addMatchers({
    toBeSigning: function () {
      return {
        compare: function (actual, expected) {
          var user = actual;

          return {
            pass: user.currentlySigningLog === expected && user.isSigning
          };
        }
      };
    }
  });
});
