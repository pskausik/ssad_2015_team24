# agent-dashboard

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.




Steps to Install:

1. Make sure latest versions of Node and Npm are installed.
   Check with node -v and npm -v
   You can download the latest version from http://nodejs.org

2. Run this to install all the dependencies globally.
   npm install -g grunt-cli bower yo generator-karma generator-angular

3. Now inside the folder, run Grunt Serve to get started.

4. To get the code that will be deployed run Grunt Build

5. Push all code to the repo.

6. This code in "dist" folder will automatically be picked up for deployment.


Steps After Grunt Build

1. Add Socket IO JS file manually to Index.html before vendors.js
2. Copy images folder to dist folder.
3. Rename dist folder to dashboard
4. Copy dashboard to gateway.
