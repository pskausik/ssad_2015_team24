'use strict';

/**
 * @ngdoc function
 * @name agentDashboardApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the agentDashboardApp
 */
angular.module('agentDashboardApp')
  .controller('HomeCtrl', function ($scope, $http, $q, $location, $timeout, $rootScope, $window) {
  	// var host = "http://localhost";
  	// var port = ":3000";

  	// TODO
  	/*
		1. Show search results in a formatted manner
		2. Newest chat comes on the top
		3. Delete a room
		4. Store the rooms in LocalStorage
		5. Make it adaptive so it works on mobile web, experience similar to android app below 900 px.
    6. Deploy code. First deploy after search.
  	*/

  	
  	var host = "http://staging.supergenie.co";
  	var port = "";
  	var endpoint = "/api/v1/incomingmsg";

    $scope.showThinkingText = false;
    $scope.thinkingText = "IVA is thinking...";

  	$scope.guid = 0;

  	$scope.rooms = [];

  	$scope.selectedRoom = 0;

  	$scope.glued = true;

  	$scope.createNewChat = function() {
  		/*
  			() -> ()
			This function will create a room with empty roomId, increment the guid value, and show the interface for chat.
  			If there is a room that already has a null id, then remove that room and add a new one.
  		*/
  		angular.forEach($scope.rooms, function(value, key) {
  			if(value.room_id == null) {
  				$scope.rooms.splice(key, 1);
  			}
  		});
  		$scope.guid += 1;
  		var room = {
  			messages: [],
  			guid: $scope.guid,
  			room_id: null,
  			app_id: 2,
  			uid: 2,
  			category: "travel",
  			avatar_name: "Please send a message",
  			avatar_image_url: "images/logo.png",
  			inputMessage: "",
  			inputError: false,
  			created_at: (new Date()).getTime()
  		};
  		$scope.rooms.push(room);
  		$scope.selectedRoom = $scope.rooms.length - 1;
		$timeout(function() {
			angular.element('.inputMessage').trigger('focus');
		}, 0);
  		console.log($scope.rooms);
  	};

  	$scope.sendMessage = function(selectedRoom) {
  		console.log("Selected Room: " + selectedRoom);
  		if($scope.rooms[selectedRoom].inputMessage.trim() == '') {
  			$scope.rooms[selectedRoom].inputError = true;
  			return;
  		}
  		var message = {
  			app_id: $scope.rooms[selectedRoom].app_id,
  			uid: $scope.rooms[selectedRoom].uid,
  			room_id: $scope.rooms[selectedRoom].room_id,
  			guid: $scope.rooms[selectedRoom].guid,
  			category: $scope.rooms[selectedRoom].category,
  			msg: $scope.rooms[selectedRoom].inputMessage
  		};

  		console.log(message);

  		addMessageToRoom(selectedRoom, $scope.rooms[selectedRoom].inputMessage, 1, 'user');

      setThinkingText(true);

  		getReply(message, selectedRoom)
  		.then(
  			function(data) {
  				/*
  					{
  					    "guid": null,
  					    "room_id": "5603f6655e0eb00f1ea559d2",
  					    "message_id": "5603f6655e0eb00f1ea559d3",
  					    "avatar_name": "leo",
  					    "avatar_image_url": "http://staging.supergenie.co/images/avatars/leo.png",
  					    "generated_msg": "How many people will be travelling?"
  					}
  				*/
          setThinkingText(false);

  				var room = $scope.rooms[data.selectedRoom];
  				room.room_id = data.room_id;
  				room.guid = data.guid;
  				room.avatar_name = data.avatar_name;
  				room.avatar_image_url = data.avatar_image_url;
  				room.messages[room.messages.length - 1].id = data.message_id;

  				addMessageToRoom(data.selectedRoom, data.generated_msg, 1, 'agent');
  				console.log(data);
  			}, function(result) {
          setThinkingText(false);
  				console.log("Failed to get Reply, the result is: ", result);
  			});
  	};

    var setThinkingText = function(flag) {
      $scope.showThinkingText = flag;
    };

  	var addMessageToRoom = function(selectedRoom, messagetext, category, sender) {
  		if (category == 1) {
  			// This is a text message.
  			var message = {
  				msg: messagetext,
  				category: 1,
  				sender: sender,
  				created_at: (new Date).getTime(),
  				id: -1
  			}
  			$scope.rooms[selectedRoom].messages.push(message);
  		} else if(category == 2) {
  			// This is a search card message.
  		}

  		$scope.rooms[selectedRoom].inputMessage = "";
  	};


	var getReply = function(data, selectedRoom) {
  		var self = this;
    	var deferred = $q.defer();

    	var url = host + port + endpoint;

        $http({
		       method: 'post',
		       url: url,
		       data: data
		 }).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		    console.log(data);
		    data.selectedRoom = selectedRoom;
		    deferred.resolve(data);
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    
		    deferred.reject(data);
		    // Add code here that will report the error to us on mixpanel or something.
		    // Also use the status code to show different errors.
		  });

		return deferred.promise;
  	};

  	$scope.changeSelectedRoom = function(id) {
  		$scope.selectedRoom = id;
  		// setting notifications to zero, once the user has seen it.
  		// $scope.rooms[$scope.selectedChat].notification_count = 0;
  	};

  	$scope.sortChatList = function() {
  		console.log($scope.chatlist);

  		angular.forEach($scope.chatlist, function(val1, i) {
  			angular.forEach($scope.chatlist, function(val2, j) {
  				if(val1.created_at > val2.created_at) {
  					var temp = $scope.chatlist[i];
  					$scope.chatlist[i] = $scope.chatlist[j];
  					$scope.chatlist[j] = temp;
  				}
  			});
  		});

  		console.log($scope.chatlist);
  	};

  	$scope.sortListReverse = function(list) {
  		console.log(list.created_at);

  		return list.reverse();
  	};

  });
