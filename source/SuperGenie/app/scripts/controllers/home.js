'use strict';

/**
 * @ngdoc function
 * @name agentDashboardApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the agentDashboardApp
 */
angular.module('agentDashboardApp')
  .controller('HomeCtrl', function ($scope, $http, $q, $location, $timeout, $rootScope, $window, Rooms) {
  	// var host = "http://localhost";
  	// var port = ":3000";

  	// TODO
  	/*
		1. Delete a room
		2. Store the rooms in LocalStorage
		3. Add speech to text and text to speech
    */

  	$scope.rooms = Rooms.rooms;

    $scope.setSelectedIndex = function(index) {
      Rooms.changeSelectedRoom(index);
      console.log(index, Rooms.selectedRoomIndex);
      $location.path("/chat");
    }

    $scope.getMessage = function(msg) {
      if(typeof(msg) == "string") {
        return msg;
      } else {
        return "Search results";
      }
    }

    $scope.deleteRoom = function(index) {
      Rooms.deleteRoom(index);
    }

  });
