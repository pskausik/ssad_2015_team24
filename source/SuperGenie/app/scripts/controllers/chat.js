'use strict';

/**
 * @ngdoc function
 * @name agentDashboardApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the agentDashboardApp
 */
angular.module('agentDashboardApp')
  .controller('ChatCtrl', function ($scope, $http, $q, $location, $timeout, $rootScope, $window, $routeParams, Rooms, ngToast) {
  	var host = "http://staging.supergenie.co";
  	var port = "";
  	var endpoint = "/api/v1/incomingmsg";

  	$scope.showThinkingText = false;
    $scope.thinkingText = "IVA is thinking...";

    $scope.glued = true;
    $scope.showFirstMessage = false;

  	$scope.room_index = Rooms.selectedRoomIndex;
    console.log(Rooms.selectedRoomIndex, $scope.room_index);

  	$scope.inputMessage = "";
  	$scope.inputError = "";

  	$scope.room = {};

  	if($scope.room_index != null) {
  		// Clicked on an existing room.
      console.log(Rooms.rooms);
	  	$scope.room = Rooms.rooms[$scope.room_index];

  	} else {
  		// User wants to create a new room.
  	}

    $scope.sendMessage = function() {
  		if($scope.inputMessage.trim() == '') {
  			$scope.inputError = true;
  			return;
  		}
  		var message = {
  			app_id: 2,
  			uid: 2,
  			room_id: $scope.room.room_id || null,
  			guid: $scope.room.guid || null,
  			category: $scope.room.category || "travel",
  			msg: $scope.inputMessage
  		};

  		console.log(message);
  		if($scope.room.room_id) {
  			// This is not the first message since the room already exists.
  			// var room_index = Rooms.getRoomIndexByRoomId($scope.room.room_id);
  			Rooms.addMessageToRoom($scope.room_index, message.msg, 1, "user");
  		} else {
  			// This is the first message.
  			$scope.showFirstMessage = true;
  			$scope.firstMessage = message.msg;
  		}
  		//addMessageToRoom(selectedRoom, $scope.rooms[selectedRoom].inputMessage, 1, 'user');
      $scope.inputMessage = "";
    	setThinkingText(true);

  		getReply(message)
  		.then(
  			function(data) {
  				/*
  					{
  					    "guid": null,
  					    "room_id": "5603f6655e0eb00f1ea559d2",
  					    "message_id": "5603f6655e0eb00f1ea559d3",
  					    "avatar_name": "leo",
  					    "avatar_image_url": "http://staging.supergenie.co/images/avatars/leo.png",
  					    "generated_msg": "How many people will be travelling?"
  					}
  				*/
      		setThinkingText(false);

      		if(!$scope.room.room_id) {
      			console.log("CAME HERE");
      			var room_id = Rooms.createRoom(data.room_id, data.guid, 2, 2, "travel", data.avatar_name, data.avatar_image_url);
	  				$scope.room_index = Rooms.getRoomIndexByRoomId(room_id);
	  				$scope.room = Rooms.rooms[$scope.room_index];
      		}

  				if($scope.showFirstMessage) {
  					Rooms.addMessageToRoom($scope.room_index, message.msg, 1, "user");
  				}
  				
  				$scope.showFirstMessage = false;
          if(typeof(data.generated_msg) == "string"){
    				Rooms.addMessageToRoom($scope.room_index, data.generated_msg, 1, "agent");
          }
          else {
            Rooms.addMessageToRoom($scope.room_index, data.generated_msg, 2, "agent");
          }

  			}, function(result) {
      		setThinkingText(false);
  				console.log("Failed to get Reply, the result is: ", result);
          var myToast = ngToast.create({
            className: 'danger',
            content: '<p>No Match Found. Can you please repharse?</p>'
          });
          console.log(myToast);
  			});
  	};

    $scope.getArrDate = function(date) {
      if(date) {
        var index = date.indexOf('t');
        return date.substring(0, index);
      }
    };

    $scope.getDuration = function(duration) {
      // Take duration in minutes and converts to Hours and Minutes.
      var hours = duration/60;
      var minutes = duration%60;
      return hours + "h " + minutes + "m";
    };

    var setThinkingText = function(flag) {
      $scope.showThinkingText = flag;
    };

    var getReply = function(data) {
  		var self = this;
    	var deferred = $q.defer();

    	var url = host + port + endpoint;

        $http({
		       method: 'post',
		       url: url,
		       data: data
		 }).
		  success(function(data, status, headers, config) {
		    // this callback will be called asynchronously
		    // when the response is available
		    console.log(data);
		    deferred.resolve(data);
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    
		    deferred.reject(data);
		    // Add code here that will report the error to us on mixpanel or something.
		    // Also use the status code to show different errors.
		  });

  		return deferred.promise;
  	};

  	$timeout(function() {
  		angular.element('.inputMessage').trigger('focus');
  	}, 0);

  });
