angular.module('agentDashboardApp')
.factory('Rooms', function () {

    var guid = 0;

    var rooms = [];

    var selectedRoomIndex = null;

    var createRoom = function(room_id, guid, app_id, uid, category, avatar_name, avatar_image_url) {
        guid += 1;
        var room = {
            messages: [],
            guid: guid,
            room_id: room_id,
            app_id: app_id,
            uid: uid,
            category: category,
            avatar_name: avatar_name,
            avatar_image_url: avatar_image_url,
            created_at: (new Date()).getTime()
        };

        rooms.unshift(room);
        console.log(rooms, "FROM MODEL");

        return room.room_id;
    };

    var deleteRoom = function(index) {
        room.splice(index, 1);
    };

    var getRoomIndexByRoomId = function(room_id) {
        var key_final = -1;
        angular.forEach(rooms, function(value, key) {
            console.log(value, key);
            if(value.room_id == room_id) {
                console.log("Came here");
                key_final = key;
            }
        });
        return key_final;
    };

    var addMessageToRoom = function(selectedRoomIndex, messagetext, category, sender) {
        console.log(selectedRoomIndex, "AddMessage to Room");
        var message = {
            msg: messagetext,
            category: category,
            sender: sender,
            created_at: (new Date).getTime()
        }
        rooms[selectedRoomIndex].messages.push(message);
    };

    var changeSelectedRoom = function(index) {
        this.selectedRoomIndex = index;
        console.log("came here");
    };

    return {
        rooms: rooms,
        selectedRoomIndex: selectedRoomIndex,
        createRoom: createRoom,
        deleteRoom: deleteRoom,
        getRoomIndexByRoomId: getRoomIndexByRoomId,
        addMessageToRoom: addMessageToRoom,
        changeSelectedRoom: changeSelectedRoom
    };
});