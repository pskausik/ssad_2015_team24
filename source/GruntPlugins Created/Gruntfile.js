module.exports = function (grunt) {

    grunt.initConfig({
        uglify: {
            options : {
                mangle : true,
                compress :true,
                sourceMap : "dist/application.map",
                banner: "/* Siddhartha Gairola 2015*/\n"
            },
            target: {
                src: "./dest/application.js",
                dest: "./dist/application.min.js"
            }

        },
        jshint: {
        
            options: {
            
                eqeqeq: true,
                curly: true,
                undef: true
            },

            target:{

                src: "src/*.js"
            }

        },

        concat:{
            options:{
                
                separator: ";",
                banner: "/* Siddhartha Gairola 2015*/\n"

            },
            target:{

                src: ["src/application.js","src/util.js"],
                dest: "dest/application.js"
            }
        
        },

        
        watch:{

            scripts:{

                files: ["src/*.js"],
                tasks: ["jshint"]
            }
        },

        coffee:{

                options:{

                    bare:true,
                    join:false,
                    separator:";"

                },

                target:{
                
                    expand:true,
                    cwd: 'src',
                    src: './*.coffee',
                    dest: "lib/",
                    ext: ".js"

                }

            }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-coffee');

    grunt.registerTask("default",['jshint','concat','uglify']);
    grunt.registerTask("tutorial","this is an example task",function(){

        if(+new Date() %2 == 0){
        console.log("the time is even");
        }
        else{
            
            console.log("the time is odd");
        }

    });

    
    
    
    
};
